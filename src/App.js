import React, { PureComponent } from 'react'
import * as api from './services/api'
import { Modal } from './components/Modal'
import { PENTA_CARD_ICON, HEART_WHITE_ICON } from './assets/'
import { GoogleLogin, GoogleLogout } from 'react-google-login';
import './App.css';

class App extends PureComponent {
  state = {
    images: [],
    modalIsOpen: false,
    selectedImg: null,
    signedIn: false,
  }

  componentDidMount() {
    this.fetchImages()
  }

  componentWillUnmount() {
    this.signOut()
  }

  fetchImages = () => api.getPetImages().then(res => this.setState({ images: res.reverse() }))

  renderFrames = () => this.state.images.sort((a, b) => b.date - a.date).map(image => {
    console.log(image)
    return (
      <div
        className="masonry-brick"
        tabIndex="0"
        key={image.filename}
      >
        <img
          onClick={this.openModal(image)}
          src={image.url}
          className="masonry-image"
          alt=""
        />
        <div className="masonry-image-caption">
          <div className="masonry-image-caption-from">
            <p className="masonry-image-caption-p">from:</p>
            <p className="masonry-image-caption-p">{image.from}</p>
          </div>
          <div>
            <img src={HEART_WHITE_ICON} className="masonry-image-caption-heart" alt="" />
          </div>
        </div>
      </div>
    )
  })

  openModal = img => () => this.setState({ modalIsOpen: true, selectedImg: img })

  closeModal = () => this.setState({ modalIsOpen: false })

  uploadImg = (e) => {
    this.fetchImages()
    api.uploadPetImage(e)
  }

  onSignIn = (googleUser) => {
    const profile = googleUser.getBasicProfile();
    console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
    console.log('Name: ' + profile.getName());
    console.log('Image URL: ' + profile.getImageUrl());
    console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    localStorage.setItem('penta-pets-email', profile.getEmail())
    this.setState({signedIn: true})
  }
  

  signOut = () => {
    var auth2 = window.gapi.auth2.getAuthInstance();
    auth2.signOut().then(() => {
      localStorage.removeItem('penta-pets-email')
      this.setState({signedIn: false})
    });
  }
  render() {
    const { modalIsOpen, selectedImg } = this.state
    return (
      <div className="App">
        <Modal
          modalIsOpen={modalIsOpen}
          closeModal={this.closeModal}
          selectedImg={selectedImg}
        />
        <header className="header">
          <div className="penta-card">
            <img src={PENTA_CARD_ICON} className="penta-card-img" alt="" />
          </div>
          <p className="header-title">
              Penta Pets
          </p>
          <div className="header-btn-holder">
            {!this.state.signedIn ? (<GoogleLogin
              clientId="986732349843-prgeajl4ojekkanobt1tiu4ds8bi6o98.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={this.onSignIn}
              onFailure={console.log}
              isSignedIn={true}
              cookiePolicy={'single_host_origin'}
            />) :
            (<div>
              <GoogleLogout
                clientId="658977310896-knrl3gka66fldh83dao2rhgbblmd4un9.apps.googleusercontent.com"
                buttonText="Logout"
                onLogoutSuccess={this.signOut}
              />
              <label className="upload-btn">
                Upload
                <input
                  type="file"
                  name="myImage"
                  accept="image/x-png,image/gif,image/jpeg"
                  onChange={this.uploadImg}
                />

            </label>
              </div>)}
          </div>
        </header>
        <div className="App-uploader">
            <p>Working from home? Send us a picture of your pets!</p>
            
        </div>
        <div className="container">
            <div className="masonry">
                {this.renderFrames()}
            </div>
        </div>
      </div>
    );
  }
}

export default App;
