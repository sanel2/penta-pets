import axios from 'axios'

const ROOT_URL = 'https://ljhii3luah.execute-api.eu-central-1.amazonaws.com'

export const getPetImages = async () => {
    const response = await axios.get(`${ROOT_URL}/latest/get-all`)
    return response.data
}
export const uploadPetImage = async (image) => {
    const email = localStorage.getItem('penta-pets-email')
    const img = image.target.files[0]
    const config = {
        headers: { 'content-type': 'multipart/form-data' }
    }
    const { data: { url } } = await axios.get(`${ROOT_URL}/latest/get-upload-data?email=${email}`)
    return await axios.put(url, img, config)
}