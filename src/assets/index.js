import HEART_ICON from './heart.png'
import HEART_WHITE_ICON from './heart-white.png'
import PENTA_CARD_ICON from './penta-card.png'

export {
    HEART_ICON,
    HEART_WHITE_ICON,
    PENTA_CARD_ICON
}