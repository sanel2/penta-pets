import React from 'react'
import ReactModal from 'react-modal'

export const Modal = ({ modalIsOpen, closeModal, selectedImg }) => {
    console.log(selectedImg)
    return (
        <ReactModal
          isOpen={modalIsOpen}
          onRequestClose={closeModal}
          className="modal"
          contentLabel={selectedImg && selectedImg.url}
        >
            <div className="modal-container" onClick={closeModal}>
                <img className="modal-img" src={selectedImg && selectedImg.url} alt="" />
            </div>
        </ReactModal>
    )
}

